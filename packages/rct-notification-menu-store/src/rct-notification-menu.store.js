// eslint-disable-next-line
import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctNotificationMenuStore {
  @observable page = 1
  @observable counter = 5
  @observable data = [
    {
      id: 1,
      avatar: "date_range",
      activity: "Test Activity",
      event: "October 4, 2017 at 1pm",
      payment: "Credit Card Online",
      refund: "",
      isClicked: false,
      isRead: false,
      link: "#",
      timestamp: "08/31/2017 at 8:55pm",
      client: "Blake Pridham",
      type: "booking"
    },
    {
      id: 2,
      avatar: "date_range",
      activity: "Test Activity",
      event: "October 10, 2017 at 5pm",
      payment: "Office POS",
      refund: "",
      isClicked: false,
      isRead: false,
      link: "#",
      timestamp: "08/31/2017 at 8:54pm",
      client: "Kevin Adams",
      type: "booking"
    },
    {
      id: 3,
      avatar: "person_pin",
      activity: "Test Activity",
      event: "October 2, 2017 at 1pm",
      payment: "",
      refund: "",
      isClicked: true,
      isRead: false,
      link: "#",
      timestamp: "08/31/2017 at 8:53pm",
      client: "Ryan Stobie",
      type: "event",
      subtype: 'assigned'
    },
    {
      id: 4,
      avatar: "person_pin",
      activity: "Test Activity",
      event: "October 5, 2017 at 1pm",
      payment: "",
      refund: "",
      isClicked: true,
      isRead: false,
      link: "#",
      timestamp: "08/31/2017 at 8:52pm",
      client: "Phil Burgess",
      type: "event",
      subtype: 'assigned'
    },
    {
      id: 5,
      avatar: "date_range",
      activity: "Test Activity",
      event: "October 1, 2017 at 4pm",
      payment: "Credit Card Online",
      refund: "",
      isClicked: false,
      isRead: false,
      link: "#",
      timestamp: "08/31/2017 at 8:51pm",
      client: "Blake Pridham",
      type: "booking"
    },
  ]
  @observable open = false
  @observable element = undefined
  @observable inProgress = false

  @action resetCounter() {
    this.counter = 0
  }
  @action reset() {
    this.data = []
    this.page = 1
  }
  @action setOpen(value) {
    this.open = value
  }
  @action setElement(value) {
    this.element = value
  }
  @action appendData(value) {
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page++;
    console.log(this.data)
  }
  @action async fetchData({ operationId }) {
    this.inProgress = true
    try {
      console.log('fetchData')
      const { body: { list } } = await swaggerClientStore.client.apis.Account[operationId]({
        pageSize: 10,
        page: this.page,
      })
      if (list && list.length) {
        // list.map( notification => {
        //   const { client, activity, payment, timestamp, refund, color, event, guide } = notification
        // })
        runInAction(() => {
          this.appendData(list)
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctNotificationMenuStore()
