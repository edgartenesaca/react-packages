# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Bug Fixes

* **minor:** minor ([9f7a286](https://gitlab.com/4geit/react-packages/commit/9f7a286))


### Features

* **notification-menu-store:** add pagination to load more button and progress circular ([d9984b0](https://gitlab.com/4geit/react-packages/commit/d9984b0))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **notification-menu-component:** fix horizontal scroll and set id to have unique prop key ([eaedda7](https://gitlab.com/4geit/react-packages/commit/eaedda7))




<a name="1.59.0"></a>
# [1.59.0](https://gitlab.com/4geit/react-packages/compare/v1.58.1...v1.59.0) (2017-10-05)


### Features

* **rct-notification-menu-component:** add badge as well as avatar icon ([7af3075](https://gitlab.com/4geit/react-packages/commit/7af3075))




<a name="1.58.1"></a>
## [1.58.1](https://gitlab.com/4geit/react-packages/compare/v1.58.0...v1.58.1) (2017-10-05)


### Bug Fixes

* **notification-menu-component:** fixed notification content ([b92b0ed](https://gitlab.com/4geit/react-packages/commit/b92b0ed))




<a name="1.52.1"></a>
## [1.52.1](https://gitlab.com/4geit/react-packages/compare/v1.52.0...v1.52.1) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add isRead logic for notification background ([7dc67a8](https://gitlab.com/4geit/react-packages/commit/7dc67a8))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.51.0"></a>
# [1.51.0](https://gitlab.com/4geit/react-packages/compare/v1.50.0...v1.51.0) (2017-10-04)


### Features

* **notification-menu-store:** create notification menu store ([7d53464](https://gitlab.com/4geit/react-packages/commit/7d53464))
