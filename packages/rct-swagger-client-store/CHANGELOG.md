# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)


### Features

* **swagger-client-store:** add new method ([452df7d](https://gitlab.com/4geit/react-packages/commit/452df7d))




<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)


### Bug Fixes

* **swagger-client-store:** add missing basePath update ([bd0f147](https://gitlab.com/4geit/react-packages/commit/bd0f147))




<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)


### Features

* **swagger-client-store:** add swaggerUrl prop ([dbb173b](https://gitlab.com/4geit/react-packages/commit/dbb173b))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)


### Bug Fixes

* **template:** fix minor issue ([a290cb9](https://gitlab.com/4geit/react-packages/commit/a290cb9))




<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-swagger-client-store

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-swagger-client-store
