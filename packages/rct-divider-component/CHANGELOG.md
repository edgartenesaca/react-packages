# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-divider-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-divider-component

<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-divider-component

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-divider-component

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-divider-component

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)


### Features

* **packages:** import other packages ([8d980fa](https://gitlab.com/4geit/react-packages/commit/8d980fa))
