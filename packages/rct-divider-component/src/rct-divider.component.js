import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

import './rct-divider.component.css'

@withStyles(theme => ({
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  // TBD
})
@defaultProps({
  lineColor: '#d8d9d9'
})
export default class RctDividerComponent extends Component {
  render() {
    // const { ... } = this.props
    return (
      <hr style={{
        margin: "0 10px",
        border: "none",
        borderBottomColor: lineColor,
        borderBottomStyle: "dashed",
        borderBottomWidth: "2px"
      }}/>
    )
  }
}
