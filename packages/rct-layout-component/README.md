# @4geit/rct-layout-component [![npm version](//badge.fury.io/js/@4geit%2Frct-layout-component.svg)](//badge.fury.io/js/@4geit%2Frct-layout-component)

---

a generic layout component to place other components of a react app properly

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-layout-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-layout-component) package manager using the following command:

```bash
npm i @4geit/rct-layout-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-layout-component
```

2. Depending on where you want to use the component you will need to import the class `RctLayoutComponent` to your project JS file as follows:

```js
import RctLayoutComponent from '@4geit/rct-layout-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctLayoutComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctLayoutComponent from '@4geit/rct-layout-component'
// ...
const App = () => (
  <div className="App">
    <RctLayoutComponent/>
  </div>
)
```
