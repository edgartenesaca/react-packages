import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
import { observable, action, toJS } from 'mobx'
import { inject, observer } from 'mobx-react'

import './rct-login.component.css'

import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import VpnKeyIcon from 'material-ui-icons/VpnKey'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import Snackbar from 'material-ui/Snackbar'
import Typography from 'material-ui/Typography'
import { FormControlLabel } from 'material-ui/Form'
import { withRouter } from 'react-router-dom'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'

@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('authStore', 'notificationStore')
@withRouter
@observer
@propTypes({
  cardWidth: PropTypes.string,
  redirectTo: PropTypes.string.isRequired,
})
@defaultProps({
  redirectTo: '/',
})
export default class RctLoginComponent extends Component {
  componentWillUnmount() {
    this.props.authStore.reset()
  }
  handleEmailChange(event) {
    this.props.authStore.setEmail(event.target.value)
  }
  handlePasswordChange(event) {
    this.props.authStore.setPassword(event.target.value)
  }
  handleRememberChange(event, isInputChecked) {
    this.props.authStore.setRemember(isInputChecked)
  }
  async handleSubmitForm(event) {
    event.preventDefault()
    await this.props.authStore.login()
    this.props.history.replace(this.props.redirectTo)
  }
  render() {
    const { cardWidth } = this.props
    const { values, error, inProgress } = this.props.authStore
    return (
      <div style={{
        width: cardWidth ? cardWidth : '100%',
      }} >
        <form onSubmit={ this.handleSubmitForm.bind(this) }>
          <Card>
            <CardHeader
              title="Login"
              subheader="Get connected to your account"
              avatar={ <VpnKeyIcon/> }
            />
            <CardContent>
              <Typography type="body1" gutterBottom>
                Fill out the fields above with your credentials.
              </Typography>
              <div>
                <TextField
                  label="Email"
                  type="email"
                  id="email"
                  name="email"
                  value={ values.email }
                  onChange={ this.handleEmailChange.bind(this) }
                  fullWidth={true}
                  required={true}
                  autoFocus={true}
                />
              </div>
              <div>
                <TextField
                  label="Password"
                  type="password"
                  id="password"
                  name="password"
                  value={ values.password }
                  onChange={ this.handlePasswordChange.bind(this) }
                  fullWidth={true}
                  required={true}
                />
              </div>
              <div>
                <FormControlLabel
                  control={
                    <Checkbox
                      id="remember"
                      name="remember"
                      checked={ values.remember }
                      onChange={ this.handleRememberChange.bind(this) }
                    />
                  }
                  label="Remember me"
                />
              </div>
            </CardContent>
            <CardActions>
              <Grid container justify="center">
                <Grid item>
                  <Button
                    raised
                    color="accent"
                    type="submit"
                    disabled={ inProgress }
                  >Sign in</Button>
                </Grid>
              </Grid>
            </CardActions>
          </Card>
        </form>
      </div>
    )
  }
}
