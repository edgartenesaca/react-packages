import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

class RctNotificationStore {
  @observable message = undefined
  @observable duration = 4000
  @observable open = false

  @action setDuration(value) {
    this.duration = value
  }
  @action newMessage(value) {
    this.message = value
    this.open = true
  }
  @action close() {
    this.open = false
  }
  // @computed get dynamicVar1() {
  //   return this.var1.filter(i => ...)
  // }
}

export default new RctNotificationStore()
