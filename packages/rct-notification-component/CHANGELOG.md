# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-notification-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-notification-component

<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.1...v1.22.0) (2017-09-08)


### Features

* **notification:** separate notification snackbar into a new component ([e528aba](https://gitlab.com/4geit/react-packages/commit/e528aba))
