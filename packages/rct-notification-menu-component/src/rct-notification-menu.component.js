import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
// eslint-disable-next-line
import classNames from 'classnames'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'
import Avatar from 'material-ui/Avatar'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Divider from 'material-ui/Divider'
import Button from 'material-ui/Button'
import Badge from 'material-ui/Badge'
import { CircularProgress } from 'material-ui/Progress'

import './rct-notification-menu.component.css'
import logo from './logo.png.js'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
  // }
  listStyle: {
    width: '100%!important',
    paddingRight: '0px!important',
  }
}))
@withWidth()
@inject('notificationMenuStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  operationId: PropTypes.string,
  notificationPagePath: PropTypes.string,
})
@defaultProps({
  // TBD
})

export default class RctNotificationMenuComponent extends Component {
  async componentWillMount() {
    // const { notificationMenuStore, operationId } = this.props
    // await notificationMenuStore.fetchData({ operationId })
  }
  async handleLoadMore() {
    const { notificationMenuStore, operationId } = this.props
    await notificationMenuStore.fetchData({ operationId })
  }
  handleClick(event) {
    this.props.notificationMenuStore.setOpen(true)
    this.props.notificationMenuStore.setElement(event.currentTarget)
    this.props.notificationMenuStore.resetCounter()
  }
  handleRequestClose() {
    this.props.notificationMenuStore.setOpen(false)
  }
  render() {
    const { notificationMenuStore, notificationPagePath } = this.props
    const typePhrases = {
      booking: 'booked',
      adjustment: 'booking adjusted for',
      refund: 'refunded for',
      'partially refunded': 'partially refunded for',
      reservation: 'reserved',
    }
    const { inProgress } = notificationMenuStore
    return (
      <div>
        { this.props.notificationMenuStore.counter > 0 && (
          <Badge badgeContent={this.props.notificationMenuStore.counter} color="accent">
            <Avatar src={ logo }
              onClick={this.handleClick.bind(this)}
            />
          </Badge>
        )}
        { this.props.notificationMenuStore.counter === 0 && (
          <Avatar src={ logo }
            onClick={this.handleClick.bind(this)}
          />
        )}
        <Menu
          className="menu"
          id='long-menu'
          anchorEl={notificationMenuStore.element}
          open={notificationMenuStore.open} onRequestClose={this.handleRequestClose.bind(this)}
        >
          <MenuItem style={{ background: 'white'}}>
            <Grid container justify='space-between'>
              <Grid item>
                <Typography type='title'>
                  Notifications
                </Typography>
              </Grid>
              <Grid item>
                <Button color='primary' component='a' href={ notificationPagePath }>
                  See all
                </Button>
              </Grid>
            </Grid>
          </MenuItem>
          <Divider />
          {notificationMenuStore.data.map(({ id, avatar, activity, event, payment, refund, isClicked, link, timestamp, client, type, subtype }) => (
            <div>
              <MenuItem
                key={ id }
                onClick={this.handleRequestClose.bind(this)}
                style={{ background: !isClicked ? '#e6e6e6' : 'none' , height: 60 }}
              >
                <Grid container align='center'>
                  <Grid item>
                    <Avatar>
                      <Icon>{ avatar }</Icon>
                    </Avatar>
                  </Grid>
                  <Grid item xs>
                    <Grid container direction='column'>
                      <Grid item>
                        <Typography>
                          { type === 'event' && (
                            <span>Guide, &nbsp;</span>
                          )}
                          { type != 'cancellation' && (
                            <a href='#' style={{textDecoration: 'none', color: '#2196f3'}}>{ client } &nbsp;</a>
                          )}
                          { subtype && (
                            <span>was { subtype } to &nbsp;</span>
                          )}
                          { type in typePhrases && (
                          <span>{ typePhrases[type] } &nbsp;</span>
                          )}
                          { activity }
                          &nbsp;
                          <span>on &nbsp;</span>
                          { event }
                          { type === 'cancellation' && (
                            <span>cancelled</span>
                          )}
                          <span>. &nbsp;</span>
                          { payment.length > 0 && (
                            <span style={{ color: 'green' }}>{ payment }</span>
                          )}
                          { payment.length > 0 && type === 'reservation' && (
                            <span style={{ color: 'yellow' }}>{ payment }</span>
                          )}
                          { refund.length > 0 && (
                            <span style={{ color: 'red' }}>{ refund }</span>
                          )}
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Grid container justify='flex-end' align='center' spacing={40}>
                          <Grid item>
                            <Button
                              component='a'
                              href={ link }
                              color='primary'
                            >
                              View { type }
                            </Button>
                          </Grid>
                          <Grid item>
                            <Typography type='caption'>
                              { timestamp }
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </MenuItem>
              <Divider />
            </div>
          ))}
          <MenuItem>
            <Grid container justify='center'>
              <Grid item>
                { inProgress && (
                  <CircularProgress/>
                ) }
                { !inProgress && (
                  <Button color='primary' onClick={ this.handleLoadMore.bind(this) }>Load more</Button>
                )}
              </Grid>
            </Grid>
          </MenuItem>
        </Menu>
      </div>
    )
  }
}
