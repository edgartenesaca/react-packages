# @4geit/rct-event-list-component [![npm version](//badge.fury.io/js/@4geit%2Frct-event-list-component.svg)](//badge.fury.io/js/@4geit%2Frct-event-list-component)

---

the list of events sorted by date to use inside a right-side-menu

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-event-list-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-event-list-component) package manager using the following command:

```bash
npm i @4geit/rct-event-list-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-event-list-component
```

2. Depending on where you want to use the component you will need to import the class `RctEventListComponent` to your project JS file as follows:

```js
import RctEventListComponent from '@4geit/rct-event-list-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctEventListComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctEventListComponent from '@4geit/rct-event-list-component'
// ...
const App = () => (
  <div className="App">
    <RctEventListComponent/>
  </div>
)
```
