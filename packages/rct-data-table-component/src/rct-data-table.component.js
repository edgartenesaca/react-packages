import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Table, { TableBody, TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui/Table'
import Menu, { MenuItem } from 'material-ui/Menu'
import Switch from 'material-ui/Switch'
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography'
import Checkbox from 'material-ui/Checkbox'
import Grid from 'material-ui/Grid'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete';
import AddIcon from 'material-ui-icons/Add';
import EditIcon from 'material-ui-icons/Edit';
import ImportExportIcon from 'material-ui-icons/ImportExport'
import ViewColumnIcon from 'material-ui-icons/ViewColumn'
import FilterListIcon from 'material-ui-icons/FilterList'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'

import './rct-data-table.component.css'

@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('dataTableStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  listOperationId: PropTypes.string.isRequired,
  addOperationId: PropTypes.string.isRequired,
  updateOperationId: PropTypes.string.isRequired,
  deleteOperationId: PropTypes.string.isRequired,
  enabledColumns: PropTypes.array,
  // TBD
})
@defaultProps({
  title: 'Data Table',
  enabledColumns: [],
  // TBD
})
export default class RctDataTableComponent extends Component {
  async componentWillMount() {
    const { listOperationId, enabledColumns, dataTableStore } = this.props
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
  }
  componentWillUnmount() {
    this.props.dataTableStore.reset()
  }
  handleOpenAddDialogClick() {
    const { dataTableStore } = this.props
    dataTableStore.mutableColumns.map(({ name }) => {
      dataTableStore.setAddDialogField(name, '')
    })
    dataTableStore.setAddDialogOpen(true)
  }
  handleOpenEditDialogClick = (item) => () => {
    const { dataTableStore } = this.props
    dataTableStore.setEditDialogOpen(true)
    dataTableStore.setEditDialogItem(item)
  }
  handleAddDialogInputChange({ target: { name, value } }) {
    console.log('handleAddDialogInputChange')
    this.props.dataTableStore.setAddDialogField(name, value)
  }
  handleEditDialogInputChange({ target: { name, value } }) {
    console.log('handleEditDialogInputChange')
    this.props.dataTableStore.setEditDialogField(name, value)
  }
  async handleAddClick() {
    const { dataTableStore, enabledColumns, listOperationId, addOperationId } = this.props
    const { addDialogFields: body } = dataTableStore
    await dataTableStore.addItem({
      addOperationId,
      body,
    }).then(() => {
      this.handleAddDialogRequestClose()
    })
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
  }
  async handleEditClick() {
    const { dataTableStore, enabledColumns, listOperationId, updateOperationId } = this.props
    const { editDialogFields: body, editDialogItem: { id } } = dataTableStore
    await dataTableStore.editItem({
      updateOperationId,
      id,
      body,
    })
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
    this.handleEditDialogRequestClose()
  }
  handleOpenColumnsMenuClick(event) {
    const { dataTableStore } = this.props
    dataTableStore.setColumnsMenuOpen(true)
    dataTableStore.setColumnsMenuElement(event.currentTarget)
  }
  handleColumnsMenuRequestClose() {
    this.props.dataTableStore.setColumnsMenuOpen(false)
  }
  handleAddDialogRequestClose() {
    this.props.dataTableStore.setAddDialogOpen(false)
  }
  handleEditDialogRequestClose() {
    this.props.dataTableStore.setEditDialogOpen(false)
  }
  handleColumnItemChange(item) {
    return (event, checked) => item.setEnabled(checked)
  }

  handleRemoveSnackbarOpen = ({ id }) => () => {
    const { dataTableStore } = this.props
    dataTableStore.setRemoveSnackbarOpen(true)
    dataTableStore.setRemoveId(id)
  }
  async handleRemoveSnackbarRequestClose(event, reason) {
    const { dataTableStore, enabledColumns, listOperationId, deleteOperationId } = this.props
    const { removeId } = dataTableStore
    if (reason === 'timeout') {
      console.log(`remove id ${removeId}`)
      await dataTableStore.removeItem({ id: removeId, deleteOperationId })
      await dataTableStore.fetchData({ listOperationId, enabledColumns })
    }
    dataTableStore.setRemoveSnackbarOpen(false)
    dataTableStore.setRemoveId(undefined)
  }

  render() {
    const { columnsMenuOpen, columnsMenuElement, addDialogOpen, editDialogOpen, addDialogFields, editDialogFields, editDialogItem, removeSnackbarOpen, columns, enabledColumns, mutableColumns, data } = this.props.dataTableStore
    const { title, classes } = this.props

    if (!this.props.dataTableStore.data.length) {
      return (
        <Typography>Loading datatable...</Typography>
      )
    }
    return (
      <div>
        <Paper>
          <Toolbar>
            {/* title */}
            <Typography type="title" color="inherit" style={{
              flex: 1
            }}>
              { title }
            </Typography>
            {/* add button */}
            <IconButton onClick={ this.handleOpenAddDialogClick.bind(this) }>
              <AddIcon/>
            </IconButton>
            {/* import button */}
            <IconButton>
              <ImportExportIcon/>
            </IconButton>
            {/* columns button */}
            <IconButton
              onClick={ this.handleOpenColumnsMenuClick.bind(this) }
            >
              <ViewColumnIcon/>
            </IconButton>
            {/* columns menu */}
            <Menu
              anchorEl={ columnsMenuElement }
              open={ columnsMenuOpen }
              onRequestClose={ this.handleColumnsMenuRequestClose.bind(this) }
              className="menu"
            >
              { columns.map((item) => (
                <MenuItem key={ item.name } >
                  <Grid container align="center">
                    <Grid item xs>
                      <Typography>{ item.name }</Typography>
                    </Grid>
                    <Grid item>
                      <Switch
                        checked={ item.enabled }
                        onChange={ this.handleColumnItemChange(item) }
                      />
                    </Grid>
                  </Grid>
                </MenuItem>
              )) }
            </Menu>
            {/* delete button */}
            <IconButton>
              <DeleteIcon/>
            </IconButton>
            {/* add form dialog */}
            <Dialog open={ addDialogOpen } onRequestClose={ this.handleAddDialogRequestClose.bind(this) }>
              <DialogTitle>Add a new item</DialogTitle>
              <DialogContent>
                <DialogContentText>Complete the following form and click on the Add button in order to add a new item.</DialogContentText>
                { mutableColumns.map(({ name }) => {
                  return (
                    <TextField
                      key={ name }
                      name={ name }
                      label={ name }
                      type="text"
                      fullWidth
                      onChange={ this.handleAddDialogInputChange.bind(this) }
                    />
                  )
                })}
              </DialogContent>
              <DialogActions>
                <Button onClick={ this.handleAddDialogRequestClose.bind(this) }>Cancel</Button>
                <Button onClick={ this.handleAddClick.bind(this) } color="primary">Add</Button>
              </DialogActions>
            </Dialog>
            {/* add form dialog */}
            <Dialog open={ editDialogOpen } onRequestClose={ this.handleEditDialogRequestClose.bind(this) }>
              <DialogTitle>Edit an item</DialogTitle>
              <DialogContent>
                <DialogContentText>You can edit the fields below and proceed with the button Update.</DialogContentText>
                { mutableColumns.map(({ name }) => {
                  return (
                    <TextField
                      key={ name }
                      name={ name }
                      label={ name }
                      type="text"
                      fullWidth
                      value={ editDialogItem && editDialogItem[name] }
                    />
                  )
                })}
              </DialogContent>
              <DialogActions>
                <Button onClick={ this.handleEditDialogRequestClose.bind(this) }>Cancel</Button>
                <Button onClick={ this.handleEditClick.bind(this) } color="primary">Update</Button>
              </DialogActions>
            </Dialog>
          </Toolbar>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox/>
                </TableCell>
                { enabledColumns.map((item, index) => (
                  <TableCell key={ index }>{item.name}</TableCell>
                )) }
                <TableCell>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              { data.map(item => {
                const { id } = item
                return (
                  <TableRow hover key={ id } >
                    <TableCell padding="checkbox">
                      <Checkbox/>
                    </TableCell>
                    { enabledColumns.map((column, colIndex) => (
                      <TableCell key={ colIndex } >{ item[column.name] }</TableCell>
                    )) }
                    <TableCell>
                      {/* edit button */}
                      <IconButton onClick={ this.handleOpenEditDialogClick(item) }>
                        <EditIcon/>
                      </IconButton>
                      {/* delete button */}
                      <IconButton onClick={ this.handleRemoveSnackbarOpen({ id })}>
                        <DeleteIcon/>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Paper>
        <Snackbar
          open={ removeSnackbarOpen }
          autoHideDuration={ 5000 }
          message={ <span>Item removed!</span> }
          onRequestClose={ this.handleRemoveSnackbarRequestClose.bind(this) }
          action={[
            <Button key="undo" color="accent" dense onClick={ this.handleRemoveSnackbarRequestClose.bind(this) }>Undo</Button>
          ]}
        />
      </div>
    )
  }
}
