/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */

import { configure, setAddon } from '@storybook/react'
import infoAddon, { setDefaults } from '@storybook/addon-info'

// addon-info
setDefaults({
  inline: false,
  maxPropsIntoLine: 1,
  maxPropObjectKeys: 10,
  maxPropArrayLength: 10,
  maxPropStringLength: 100,
})
setAddon(infoAddon)

// fix: https://github.com/storybooks/storybook/issues/1305#issuecomment-309245947
import PropTypes from 'prop-types'
import PropVal from '@storybook/addon-info/dist/components/PropVal'
PropVal.propTypes = {
  ...PropVal.propTypes,
  maxPropObjectKeys: PropTypes.number,
  maxPropArrayLength: PropTypes.number,
  maxPropStringLength: PropTypes.number,
  val: PropTypes.any,
};

function loadStories() {
  require('../stories')
}

configure(loadStories, module)
